package com.happymilk.service;

import java.util.List;
import java.util.logging.Logger;

import com.happymilk.dao.CustomerDAO;
import com.happymilk.domainobject.CustomerDO;
import com.happymilk.exception.AppException;

public class CustomerService {
	static Logger logger = Logger.getLogger(CustomerService.class.getName());
	
	public CustomerDO persist(CustomerDO customerDO) throws AppException {
		return new CustomerDAO().persist(customerDO);
	}

	public List<CustomerDO> retrieveById(Long Id) throws AppException {
		return new CustomerDAO().retrieveById(Id);
	}
	
	public List<CustomerDO> retrieveByBarCode(String barCode) throws AppException {
		return new CustomerDAO().retrieveByBarCode(barCode);
	}
	
	public List<CustomerDO> retrieve() throws AppException {
		return new CustomerDAO().retrieve();
	}
	
	public CustomerDO update(CustomerDO customerDO) throws AppException {
		return new CustomerDAO().update(customerDO);
	}
	
	public boolean persistList(List<CustomerDO> customerDO) throws AppException {
		return new CustomerDAO().persistList(customerDO);
	}
}
