package com.happymilk.service;

import java.util.List;
import java.util.logging.Logger;

import com.happymilk.dao.UserDAO;
import com.happymilk.domainobject.UserDO;
import com.happymilk.exception.AppException;

public class UserService {
	static Logger logger = Logger.getLogger(UserService.class.getName());
	
	public UserDO persist(UserDO userDO) throws AppException {
		return new UserDAO().persist(userDO);
	}

	public List<UserDO> retriveById(Long id) throws AppException {
		return new UserDAO().retrieveById(id);
	}
	
	public List<UserDO> retrieveByEmailId(String email) throws AppException {
		return new UserDAO().retrieveByEmailId(email);
	}
	
	public List<UserDO> retrieve() throws AppException {
		return new UserDAO().retrieve();
	}
	
	public UserDO update(UserDO userDO) throws AppException {
		return new UserDAO().update(userDO);
	}
	
	public List<UserDO> retrieveForLogin(String email, String pasword) throws AppException {
		return new UserDAO().retrieveForLogin(email, pasword);
	}
}
