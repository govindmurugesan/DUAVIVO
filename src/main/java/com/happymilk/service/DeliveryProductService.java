package com.happymilk.service;

import java.util.List;
import java.util.logging.Logger;

import com.happymilk.dao.DeliveryProductDAO;
import com.happymilk.domainobject.DeliveryProductDO;
import com.happymilk.exception.AppException;

public class DeliveryProductService {
	static Logger logger = Logger.getLogger(DeliveryProductService.class.getName());
	
	public boolean persist(List<DeliveryProductDO> deliveryProductDO) throws AppException {
		return new DeliveryProductDAO().persist(deliveryProductDO);
	}
	
	public int totalNotReturnProduct(Long customerId, char status) throws AppException {
		return new DeliveryProductDAO().totalNotReturnProduct(customerId, status);
	}

	public List<DeliveryProductDO> retrieveById(Long Id) throws AppException {
		return new DeliveryProductDAO().retrieveById(Id);
	}
	
	public List<DeliveryProductDO> retrieveByIdCustBarcode(Long customerId, String barcode) throws AppException {
		return new DeliveryProductDAO().retrieveByIdCustBarcode(customerId, barcode);
	}
	
	
	
	public List<DeliveryProductDO> retrieveByCustomerId(Long customerId) throws AppException {
		return new DeliveryProductDAO().retrieveByCustomerId(customerId);
	}
	
	public List<DeliveryProductDO> retrieve() throws AppException {
		return new DeliveryProductDAO().retrieve();
	}
	
	public DeliveryProductDO update(DeliveryProductDO deliveryProductDO) throws AppException {
		return new DeliveryProductDAO().update(deliveryProductDO);
	}
	
	public boolean persistList(List<DeliveryProductDO> deliveryProductDO) throws AppException {
		return new DeliveryProductDAO().persistList(deliveryProductDO);
	}
}
