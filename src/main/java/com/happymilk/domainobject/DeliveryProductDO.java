package com.happymilk.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="deliveryproduct")
@TableGenerator(name ="deliveryproduct", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "deliveryproduct.findAll", query = "SELECT r FROM DeliveryProductDO r"),
    @NamedQuery(name = "deliveryproduct.findById", query = "SELECT r FROM DeliveryProductDO r where r.id =:id"),
    @NamedQuery(name = "deliveryproduct.findByCustomerId", query = "SELECT r FROM DeliveryProductDO r where r.customerId =:id"),
    @NamedQuery(name = "deliveryproduct.findByCustomerIdBarcode", query = "SELECT r FROM DeliveryProductDO r where r.customerId =:id and r.deliverybarcode =:barcode"),
    @NamedQuery(name = "deliveryproduct.findSum", query = "SELECT r FROM DeliveryProductDO r where r.status =:status and r.customerId =:id"),
})
public class DeliveryProductDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "deliveryproduct.findAll";
	
	public static final String FIND_BY_ID = "deliveryproduct.findById";
	
	public static final String FIND_BY_CUSTOMERID = "deliveryproduct.findByCustomerId";
	
	public static final String FIND_SUM = "deliveryproduct.findSum";
	
	public static final String FIND_BY_CUSTOMERID_BARCODE = "deliveryproduct.findByCustomerIdBarcode";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "deliveryproduct")
	private Long id;
	private Long customerId;
	private String deliverybarcode;
	private char status;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	
	private String updatedBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdon;
	
	private String createdBy;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Long customerId) {
		this.customerId = customerId;
	}

	public String getDeliverybarcode() {
		return deliverybarcode;
	}

	public void setDeliverybarcode(String deliverybarcode) {
		this.deliverybarcode = deliverybarcode;
	}

	public char getStatus() {
		return status;
	}

	public void setStatus(char status) {
		this.status = status;
	}

	public Date getUpdatedon() {
		return updatedon;
	}

	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getCreatedon() {
		return createdon;
	}

	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
}