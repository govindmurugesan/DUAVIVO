package com.happymilk.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="user")
@TableGenerator(name ="user", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "UserDO.findAll", query = "SELECT u FROM UserDO u"),
    @NamedQuery(name = "UserDO.findById", query = "SELECT u FROM UserDO u where u.id =:userid"),
    @NamedQuery(name = "UserDO.findByUserName", query = "SELECT u FROM UserDO u where u.name =:name"),
    @NamedQuery(name = "UserDO.findByEmailId", query = "SELECT u FROM UserDO u where u.email =:email"),
    @NamedQuery(name = "UserDO.findForLogin", query = "SELECT u FROM UserDO u where u.email =:email  and  u.password=:password"),
    
})
public class UserDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "UserDO.findAll";
	
	public static final String FIND_BY_ID = "UserDO.findById";
	
	public static final String FIND_BY_EMAIL = "UserDO.findByEmailId";
	
	public static final String FIND_BY_USER_NAME = "UserDO.findByUserName";
	
	public static final String FIND_FOR_LOGIN = "UserDO.findForLogin";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "user")
	private Long id;
	
	private String email;
	private String name;
	private String password;
	private Long phone;
	private String updatedby;
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	private String createdby;
	
	public UserDO() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUpdatedby() {
		return updatedby;
	}

	public void setUpdatedby(String updatedby) {
		this.updatedby = updatedby;
	}

	public Date getUpdatedon() {
		return updatedon;
	}

	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public Long getPhone() {
		return phone;
	}

	public void setPhone(Long phone) {
		this.phone = phone;
	}
}