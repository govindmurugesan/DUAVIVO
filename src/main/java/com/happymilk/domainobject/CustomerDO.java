package com.happymilk.domainobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.TableGenerator;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="customer")
@TableGenerator(name ="customer", initialValue =100101, allocationSize =1)
@NamedQueries({
    @NamedQuery(name = "customer.findAll", query = "SELECT r FROM CustomerDO r"),
    @NamedQuery(name = "customer.findById", query = "SELECT r FROM CustomerDO r where r.id =:id"),
    @NamedQuery(name = "customer.findByBarCode", query = "SELECT r FROM CustomerDO r where r.barcode =:barcode"),
})
public class CustomerDO implements Serializable {
	private static final long serialVersionUID = 1L;

	public static final String FIND_ALL = "customer.findAll";
	
	public static final String FIND_BY_ID = "customer.findById";
	
	public static final String FIND_BY_BARCODE = "customer.findByBarCode";
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY, generator = "customer")
	private Long id;
	private String name;
	private String barcode;
	private Long quantity;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date updatedon;
	
	private String updatedBy;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date createdon;
	
	private String createdBy;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBarcode() {
		return barcode;
	}

	public void setBarcode(String barcode) {
		this.barcode = barcode;
	}

	public Long getQuantity() {
		return quantity;
	}

	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}

	public Date getUpdatedon() {
		return updatedon;
	}

	public void setUpdatedon(Date updatedon) {
		this.updatedon = updatedon;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getCreatedon() {
		return createdon;
	}

	public void setCreatedon(Date createdon) {
		this.createdon = createdon;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
}