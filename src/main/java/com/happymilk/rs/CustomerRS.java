package com.happymilk.rs;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.happymilk.domainobject.CustomerDO;
import com.happymilk.service.CustomerService;
import com.happymilk.util.CommonConstants;
import com.happymilk.web.util.CommonWebUtil;
import com.happymilk.web.util.CustomerUtil;

@Controller
@RequestMapping(value = "/customer")
public class CustomerRS {

	String validation = null;
	static Logger logger = Logger.getLogger(CustomerRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			CustomerDO customerDO = new CustomerDO();
			JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
		 	if (inputJSON != null){
		 		customerDO.setName(inputJSON != null ? inputJSON.get(CommonConstants.NAME).toString() : "");
		 		customerDO.setBarcode(inputJSON != null ? inputJSON.get(CommonConstants.BARCODE).toString() : "");
		 		customerDO.setQuantity(inputJSON != null ? Long.parseLong(inputJSON.get(CommonConstants.QUANTITY).toString()):null);
		 		customerDO.setCreatedon(new Date());
		 	}
				new CustomerService().persist(customerDO);
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			//if (WebManager.authenticateSession(request)) {
				List<CustomerDO> customerList = new CustomerService().retrieve();
				respJSON = CustomerUtil.getCustomerList(customerList).toString();
			/*}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}*/
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retriveByBarCode" , method = RequestMethod.POST)
	public @ResponseBody String retriveByBarCode(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			//if (WebManager.authenticateSession(request)) {
			JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			if(inputJSON != null && !inputJSON.get(CommonConstants.BARCODE).toString().isEmpty()){
				List<CustomerDO> customerList = new CustomerService().retrieveByBarCode(inputJSON.get(CommonConstants.BARCODE).toString());
				respJSON = CustomerUtil.getCustomerList(customerList).toString();
			}
			/*}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}*/
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			//if (WebManager.authenticateSession(request)) {
				CustomerDO customerDO = new CustomerDO();
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		List<CustomerDO> customerList = new CustomerService().retrieveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
			 		customerDO = customerList.get(0);
			 		customerDO.setId(Long.parseLong(inputJSON != null ? inputJSON.get(CommonConstants.ID).toString() : ""));
			 		customerDO.setName(inputJSON != null ? inputJSON.get(CommonConstants.NAME).toString() : "");
			 		customerDO.setBarcode(inputJSON != null ? inputJSON.get(CommonConstants.BARCODE).toString() : "");
			 		customerDO.setQuantity(inputJSON != null ? Long.parseLong(inputJSON.get(CommonConstants.QUANTITY).toString()):null);
			 	}
			/*}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}*/
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
}
