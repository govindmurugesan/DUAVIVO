package com.happymilk.rs;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.happymilk.domainobject.DeliveryProductDO;
import com.happymilk.service.DeliveryProductService;
import com.happymilk.util.CommonConstants;
import com.happymilk.web.util.CommonWebUtil;
import com.happymilk.web.util.DeliveryProductUtil;

@Controller
@RequestMapping(value = "/deliverproduct")
public class DeliveryProductRS {

	String validation = null;
	static Logger logger = Logger.getLogger(DeliveryProductRS.class.getName());
	
	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
			
		try {
			List<DeliveryProductDO> deliverProductList = new ArrayList<DeliveryProductDO>();
			JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
		 	if (inputJSON != null && inputJSON.get(CommonConstants.BARCODE) != null && !inputJSON.get(CommonConstants.BARCODE).toString().isEmpty()){
		 		JSONArray resultJSONArray = new JSONArray();
		 		resultJSONArray.put(inputJSON.get(CommonConstants.BARCODE));
		 		String rec = (String) resultJSONArray.get(0).toString();
		 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
		 		for (int i = 0; i < resultJSONArray1.size(); i++) {
		 			JSONObject inputJSON1 = (JSONObject) resultJSONArray1.get(i);
		 			if(!inputJSON1.get(CommonConstants.BARCODE).toString().isEmpty()){
		 				DeliveryProductDO deliveryProductDO = new DeliveryProductDO();
			 			deliveryProductDO.setCustomerId(inputJSON != null ? Long.parseLong(inputJSON.get(CommonConstants.ID).toString()) : null);
			 			deliveryProductDO.setDeliverybarcode(inputJSON1 != null ? inputJSON1.get(CommonConstants.BARCODE).toString() : "");
			 			deliveryProductDO.setStatus('D');
			 			deliveryProductDO.setCreatedon(new Date());
			 			deliverProductList.add(deliveryProductDO);
		 			}
		 		}
		 	}
				new DeliveryProductService().persist(deliverProductList);
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		int returnProduct = 0;
		try {
			JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
		 	if (inputJSON != null && inputJSON.get(CommonConstants.PICKUPBARCODE) != null && !inputJSON.get(CommonConstants.PICKUPBARCODE).toString().isEmpty()){
		 		JSONArray resultJSONArray = new JSONArray();
		 		resultJSONArray.put(inputJSON.get(CommonConstants.PICKUPBARCODE));
		 		String rec = (String) resultJSONArray.get(0).toString();
		 		org.json.simple.JSONArray resultJSONArray1 = CommonWebUtil.getInputParamsArray(rec);
		 		for (int i = 0; i < resultJSONArray1.size(); i++) {
		 			JSONObject inputJSON1 = (JSONObject) resultJSONArray1.get(i);
		 			if(!inputJSON1.get(CommonConstants.BARCODE).toString().isEmpty()){
		 				List<DeliveryProductDO> deliveryProductList = new DeliveryProductService().retrieveByIdCustBarcode(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()), inputJSON1.get(CommonConstants.BARCODE).toString());
		 				if(deliveryProductList != null && deliveryProductList.size() > 0){
		 					DeliveryProductDO deliveryProductDO = new DeliveryProductDO();
		 					deliveryProductDO = deliveryProductList.get(0);
		 					deliveryProductDO.setStatus('R');
		 					deliveryProductDO.setUpdatedon(new Date());
		 					new DeliveryProductService().update(deliveryProductDO);
		 				}
		 			}
		 		}
		 		returnProduct = new DeliveryProductService().totalNotReturnProduct(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()), 'D');
		 	}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponseId(returnProduct).toString();
	}
	
	
	@RequestMapping(value = "/retriveTotal/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveTotal(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		int returnProduct = 0;
		try {
			//if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					returnProduct = new DeliveryProductService().totalNotReturnProduct(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()), 'D');
				}
			/*}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}*/
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponseId(returnProduct).toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			//if (WebManager.authenticateSession(request)) {
				List<DeliveryProductDO> customerList = new DeliveryProductService().retrieve();
				respJSON = DeliveryProductUtil.getDeliveryProductList(customerList).toString();
			/*}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}*/
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/retrieveByCustomerId/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retrieveByCustomerId(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			//if (WebManager.authenticateSession(request)) {
			JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
			if(inputJSON != null && !inputJSON.get(CommonConstants.BARCODE).toString().isEmpty()){
				List<DeliveryProductDO> customerList = new DeliveryProductService().retrieveByCustomerId(Long.parseLong(inputJSON.get(CommonConstants.BARCODE).toString()));
				respJSON = DeliveryProductUtil.getDeliveryProductList(customerList).toString();
			}
			/*}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}*/
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
}
