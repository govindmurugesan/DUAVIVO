package com.happymilk.rs;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.happymilk.domainobject.UserDO;
import com.happymilk.service.UserService;
import com.happymilk.util.CommonConstants;
import com.happymilk.web.util.CommonWebUtil;
import com.happymilk.web.util.UserUtil;

@Controller
@RequestMapping(value = "/user")
public class UserRS {
	
	String validation = null;
	
	static Logger logger = Logger.getLogger(UserRS.class.getName());

	

	@RequestMapping(value = "/persist", method = RequestMethod.POST)
	public @ResponseBody String add(Model model, HttpServletRequest request) {
		try {
			UserDO userDO = new UserDO();
			JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
		 	if (inputJSON != null){
				userDO.setEmail(!inputJSON.get(CommonConstants.EMAIL).toString().isEmpty() ? inputJSON.get(CommonConstants.EMAIL).toString().toString() : null);
				userDO.setUpdatedon(new Date());
				userDO.setPassword(!inputJSON.get(CommonConstants.PASSWORD).toString().isEmpty() ? inputJSON.get(CommonConstants.PASSWORD).toString().toString() : null);
				userDO.setName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString().toString() : null);
				userDO.setPhone(!inputJSON.get(CommonConstants.PHONE).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.PHONE).toString().toString()) : null);
		 	}
		 	userDO = new UserService().persist(userDO);
		}catch (Exception e) {
			e.printStackTrace();
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieve", method = RequestMethod.GET)
	public @ResponseBody String retrieve(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			//if (WebManager.authenticateSession(request)) {
				List<UserDO> userList = new UserService().retrieve();
				respJSON = UserUtil.getUserList(userList).toString();
			/*}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}*/
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	
	
	@RequestMapping(value = "/retriveById/{inputParams}", method = RequestMethod.GET)
	public @ResponseBody String retriveById(@PathVariable String inputParams, Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			//if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(inputParams);
				if(inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
					List<UserDO> userList = new UserService().retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));
					respJSON = UserUtil.getUserList(userList).toString();
				}
			/*}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}*/
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
	
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody String update(Model model, HttpServletRequest request) {
		try {
			//if (WebManager.authenticateSession(request)) {
				JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			 	if (inputJSON != null && !inputJSON.get(CommonConstants.ID).toString().isEmpty()){
			 		UserDO userDO = new UserDO();
			 		List<UserDO>userDOList = new UserService().retriveById(Long.parseLong(inputJSON.get(CommonConstants.ID).toString()));;
				 	if (userDOList != null){
				 		userDO = userDOList.get(0);
				 		userDO = userDOList.get(0);
				 		userDO.setEmail(!inputJSON.get(CommonConstants.EMAIL).toString().isEmpty() ? inputJSON.get(CommonConstants.EMAIL).toString().toString() : null);
						userDO.setUpdatedon(new Date());
						userDO.setPassword(!inputJSON.get(CommonConstants.PASSWORD).toString().isEmpty() ? inputJSON.get(CommonConstants.PASSWORD).toString().toString() : null);
						userDO.setName(!inputJSON.get(CommonConstants.NAME).toString().isEmpty() ? inputJSON.get(CommonConstants.NAME).toString().toString() : null);
						userDO.setPhone(!inputJSON.get(CommonConstants.PHONE).toString().isEmpty() ? Long.parseLong(inputJSON.get(CommonConstants.PHONE).toString().toString()) : null);
						new UserService().update(userDO);
				 	}
			 	}
/*			}else{
				return CommonWebUtil.buildAuthenticationErrorResponse(CommonConstants.AUTHENTICATION_ERROECODE).toString();
			}*/
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
		}
		return CommonWebUtil.buildSuccessResponse().toString();
	}
	
	@RequestMapping(value = "/retrieveForLogin", method = RequestMethod.POST)
	public @ResponseBody String retrieveForLogin(Model model, HttpServletRequest request) {
		String respJSON = null;
		try {
			//if (WebManager.authenticateSession(request)) {
			JSONObject inputJSON = CommonWebUtil.getInputParams(request.getParameter("inputParam").toString());
			if(inputJSON != null && !inputJSON.get(CommonConstants.EMAIL).toString().isEmpty()
					&& !inputJSON.get(CommonConstants.PASSWORD).toString().isEmpty()){
				List<UserDO> userList = new UserService().retrieveByEmailId(inputJSON.get(CommonConstants.EMAIL).toString());
				if(userList.size() > 0) {
						respJSON = UserUtil.getUserList(userList).toString();
					}
				}
			//}
		}catch (Exception e) {
			return CommonWebUtil.buildErrorResponse("").toString();
			
		}
		return respJSON != null ? respJSON.toString() : CommonWebUtil.buildErrorResponse("").toString();
	}
}