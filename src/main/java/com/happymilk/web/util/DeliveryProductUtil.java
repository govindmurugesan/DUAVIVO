package com.happymilk.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.happymilk.domainobject.DeliveryProductDO;
import com.happymilk.exception.AppException;
import com.happymilk.util.CommonConstants;

public class DeliveryProductUtil {
	
	private DeliveryProductUtil() {}
	
	public static JSONObject getDeliveryProductList(List<DeliveryProductDO> deliveryProductDOList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (DeliveryProductDO deliveryProductDO : deliveryProductDOList) {
				resultJSONArray.put(getDeliveryProductDetailObject(deliveryProductDO));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getDeliveryProductDetailObject(DeliveryProductDO deliveryProductDO)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(deliveryProductDO.getId()));
		result.put(CommonConstants.CUSTOMERID, String.valueOf(deliveryProductDO.getCustomerId()));
		result.put(CommonConstants.BARCODE, String.valueOf(deliveryProductDO.getDeliverybarcode()));
		result.put(CommonConstants.STATUS, String.valueOf(deliveryProductDO.getStatus()));
		return result;
	}
}
