package com.happymilk.web.util;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.happymilk.domainobject.CustomerDO;
import com.happymilk.exception.AppException;
import com.happymilk.util.CommonConstants;

public class CustomerUtil {
	
	private CustomerUtil() {}
	
	public static JSONObject getCustomerList(List<CustomerDO> customerList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (CustomerDO customer : customerList) {
				resultJSONArray.put(getCustomerDetailObject(customer));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getCustomerDetailObject(CustomerDO customer)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(customer.getId()));
		result.put(CommonConstants.NAME, String.valueOf(customer.getName()));
		result.put(CommonConstants.BARCODE, String.valueOf(customer.getBarcode()));
		result.put(CommonConstants.QUANTITY, String.valueOf(customer.getQuantity()));
		return result;
	}
}
