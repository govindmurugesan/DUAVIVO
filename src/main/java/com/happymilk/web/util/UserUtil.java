package com.happymilk.web.util;

import java.util.List;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.happymilk.domainobject.UserDO;
import com.happymilk.exception.AppException;
import com.happymilk.util.CommonConstants;

public class UserUtil {
	
	private UserUtil() {}
	
	public static JSONObject getUserList(List<UserDO> userList)throws AppException {
		JSONObject responseJSON = new JSONObject();
		JSONObject resultJSON = new JSONObject();
		try {
			resultJSON.put(CommonConstants.SUCCESS_FLAG, CommonConstants.TRUE);
			resultJSON.put(CommonConstants.ERRORS, "");
			JSONArray resultJSONArray = new JSONArray();
			for (UserDO user : userList) {
				resultJSONArray.put(getUserDetailObject(user));
			}
			resultJSON.put(CommonConstants.RESULTS, resultJSONArray);
			responseJSON.put(CommonConstants.RESPONSE, resultJSON);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		return responseJSON;
	}

	public static JSONObject getUserDetailObject(UserDO user)throws JSONException, AppException {
		JSONObject result = new JSONObject();
		result.put(CommonConstants.ID, String.valueOf(user.getId() != null ? user.getId() : ""));
		result.put(CommonConstants.NAME, String.valueOf(user.getName() != null ? user.getName() : ""));
		result.put(CommonConstants.EMAIL, String.valueOf(user.getEmail() != null ? user.getEmail() : ""));
		result.put(CommonConstants.PASSWORD, String.valueOf(user.getPassword() != null ? user.getPassword(): ""));
		result.put(CommonConstants.PHONE, String.valueOf(user.getPhone() != null ? user.getPhone(): ""));
		return result;
	}
	
	public static char[] geek_Password(int len)
    {
        // A strong password has Cap_chars, Lower_chars,
        // numeric value and symbols. So we are using all of
        // them to generate our password
        String Capital_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String Small_chars = "abcdefghijklmnopqrstuvwxyz";
        String numbers = "0123456789";
                String symbols = "!@#$%^&*_=+-/.?<>)";
 
 
        String values = Capital_chars + Small_chars +
                        numbers + symbols;
 
        // Using random method
        Random rndm_method = new Random();
 
        char[] password = new char[len];
 
        for (int i = 0; i < len; i++)
        {
            // Use of charAt() method : to get character value
            // Use of nextInt() as it is scanning the value as int
            password[i] =
              values.charAt(rndm_method.nextInt(values.length()));
 
        }
        return password;
    }
}
