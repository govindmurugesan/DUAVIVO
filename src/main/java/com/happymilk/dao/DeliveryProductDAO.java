package com.happymilk.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.happymilk.domainobject.DeliveryProductDO;
import com.happymilk.exception.AppException;
import com.happymilk.exception.CustomPropertyManager;
import com.happymilk.exception.ExceptionConstant;
import com.happymilk.util.CommonConstants;
import com.happymilk.util.PersistenceUnitNames;
import com.happymilk.util.SessionManager;
import com.happymilk.util.TransactionManager;

public class DeliveryProductDAO {
	static Logger logger = Logger.getLogger(DeliveryProductDAO.class.getName());
	private EntityManager em = null;
	
	public boolean persist(List<DeliveryProductDO> deliveryProductDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && deliveryProductDO != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				for (DeliveryProductDO deliveryProductDO2 : deliveryProductDO) {
					em.persist(deliveryProductDO2);
				}
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return true;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<DeliveryProductDO> retrieve() throws AppException {
		List<DeliveryProductDO> projectList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(DeliveryProductDO.FIND_ALL);
				projectList = (List<DeliveryProductDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return projectList;
	}
	
	@SuppressWarnings("unchecked")
	public List<DeliveryProductDO> retrieveById(Long Id) throws AppException {
		List<DeliveryProductDO> projectList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(DeliveryProductDO.FIND_BY_ID);
				q.setParameter(CommonConstants.ID, Id);
				projectList = (List<DeliveryProductDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return projectList;
	}
	
	@SuppressWarnings("unchecked")
	public int totalNotReturnProduct(Long Id, char status) throws AppException {
		List<DeliveryProductDO> projectList = new ArrayList<DeliveryProductDO>();
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(DeliveryProductDO.FIND_SUM);
				q.setParameter(CommonConstants.ID, Id);
				q.setParameter(CommonConstants.STATUS, status);
				projectList = (List<DeliveryProductDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return projectList.size();
	}
	
	@SuppressWarnings("unchecked")
	public List<DeliveryProductDO> retrieveByIdCustBarcode(Long Id, String barcode) throws AppException {
		List<DeliveryProductDO> projectList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(DeliveryProductDO.FIND_BY_CUSTOMERID_BARCODE);
				q.setParameter(CommonConstants.BARCODE, barcode);
				q.setParameter(CommonConstants.ID, Id);
				projectList = (List<DeliveryProductDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return projectList;
	}
	
	@SuppressWarnings("unchecked")
	public List<DeliveryProductDO> retrieveByCustomerId(Long customerId) throws AppException {
		List<DeliveryProductDO> projectList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(DeliveryProductDO.FIND_BY_CUSTOMERID);
				q.setParameter(CommonConstants.ID, customerId);
				projectList = (List<DeliveryProductDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return projectList;
	}
	
	public DeliveryProductDO update(DeliveryProductDO DeliveryProductDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.merge(DeliveryProductDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return DeliveryProductDO;
	}
	
	public boolean persistList(List<DeliveryProductDO> deliveryProductDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && deliveryProductDO != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				for (DeliveryProductDO deliveryProductDO2 : deliveryProductDO) {
					em.persist(deliveryProductDO2);
				}
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			eException.printStackTrace();
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return true;
	}

}
