package com.happymilk.dao;

import java.util.List;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import com.happymilk.domainobject.UserDO;
import com.happymilk.exception.AppException;
import com.happymilk.exception.CustomPropertyManager;
import com.happymilk.exception.ExceptionConstant;
import com.happymilk.util.CommonConstants;
import com.happymilk.util.PersistenceUnitNames;
import com.happymilk.util.SessionManager;
import com.happymilk.util.TransactionManager;

public class UserDAO {
	static Logger logger = Logger.getLogger(UserDAO.class.getName());
	private EntityManager em = null;
	
	public UserDO persist(UserDO userDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null && userDO != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.persist(userDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			throw new AppException(ExceptionConstant._91010, CustomPropertyManager.getProperty(ExceptionConstant._91010), eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return userDO;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<UserDO> retrieve() throws AppException {
		List<UserDO> userList = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(UserDO.FIND_ALL);
				userList = (List<UserDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return userList;
	}
	
		
	@SuppressWarnings("unchecked")
	public List<UserDO> retrieveById(Long id) throws AppException {
		List<UserDO> user = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(UserDO.FIND_BY_ID);
				q.setParameter(CommonConstants.USERID, id);
				user = (List<UserDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return user;
	}
	
	@SuppressWarnings("unchecked")
	public List<UserDO> retrieveByEmailId(String email) throws AppException {
		List<UserDO> user = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(UserDO.FIND_BY_EMAIL);
				q.setParameter(CommonConstants.EMAIL, email);
				user = (List<UserDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return user;
	}

	public UserDO update(UserDO userDO) throws AppException {
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				TransactionManager.beginTransaction(em);
				em.merge(userDO);
				TransactionManager.commitTransaction();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return userDO;
	}
	
	@SuppressWarnings("unchecked")
	public List<UserDO> retrieveForLogin(String email,String password) throws AppException {
		List<UserDO> user = null;
		try {
			em = SessionManager.createManager(PersistenceUnitNames.PERSISTENCE_UNIT_NAME);
			if (em != null) {
				logger.info("Entity Manager is not null");
				Query q = em.createNamedQuery(UserDO.FIND_FOR_LOGIN);
				q.setParameter(CommonConstants.EMAIL, email);
				q.setParameter(CommonConstants.PASSWORD, password);
				q.setParameter(CommonConstants.STATUS, CommonConstants.ACTIVE);
				user = (List<UserDO>) q.getResultList();
			}
		} catch (Exception eException) {
			logger.info(eException.getMessage());
			throw new AppException(ExceptionConstant._91010,CustomPropertyManager.getProperty(ExceptionConstant._91010),eException);
		} finally {
			SessionManager.closeEntityManager(em);
		}
		return user;
	}
}
